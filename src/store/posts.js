export default {
    state: {
        posts: [],
        loader: true
    },
    mutations: {
        SET_POSTS(state, posts) {
            state.posts = posts;
            state.loader = false;
        },

        ADD_POST(state, post) {
            state.posts.unshift(post);
        },

        DELETE_POST(state, id) {
            state.posts = state.posts.filter((post) => post.id !== id);
        },

        SAVE_NEW_DATA(state, { id, newPostData }) {
            console.log(id, newPostData);
            state.posts.filter((post) => {
                if (post.id === id) {
                    post.title = newPostData.title !== '' ? newPostData.title : post.title
                    post.body = newPostData.body !== '' ? newPostData.body : post.body
                }
            })
        }
    },
    actions: {
        getPosts({ commit }) {
            fetch('https://jsonplaceholder.typicode.com/posts?_limit=5')
                .then((response) => response.json())
                .then((json) =>
                    commit('SET_POSTS', json)
                );
        }

    },
    getters: {
        allPosts(state) {
            return state.posts
        }
    }
}